package task05_Lambdas_StreamsFourTask;

import java.nio.file.Files;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class WordsRealiation implements StringForStream {

    @Override
    public void uniqueWords(String words) {
        Set<String> uniqueWords = new HashSet<String>();
        uniqueWords = Stream.of(words.split(" ")).sorted().collect(Collectors.toSet());
        int countOfWords = (int) uniqueWords.stream().count();
        System.out.println("Unique sorted words: ");
        uniqueWords.forEach(p -> {
            System.out.print(p + " ");
        });
        System.out.println("\nCount of unique words: " + countOfWords);
        System.out.println("\nCount of characters in words:");
        Map<Character, Integer> countOfChar = new TreeMap<>();
        for (Character value : words.toCharArray()) {
            if (countOfChar.containsKey(value)) {
                Integer total = countOfChar.get(value);
                countOfChar.put(value, total + 1);
            } else {
                countOfChar.put(value, 1);
            }
        }
        System.out.println(countOfChar);
    }
}
