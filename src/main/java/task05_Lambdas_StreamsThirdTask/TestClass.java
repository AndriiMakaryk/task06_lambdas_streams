package task05_Lambdas_StreamsThirdTask;

import java.util.ArrayList;
import java.util.List;

public class TestClass {
    public static void main(String[] args) {
        StreamsMethods methods = new MethodsImplement();
        List<Integer> listForTest = new ArrayList<Integer>();
        for (int i = 0; i <= 20; i++) {
            int value = (int)(15+ Math.random()*25);
            listForTest.add(value);
        }
        System.out.println("Random generated values: ");
        for (Integer element : listForTest) {
            System.out.print(element + " ");
        }
        System.out.println();
        int average=methods.averageOfListValues(listForTest);
        System.out.println("Average of those values: "+average);
        int maxValue=methods.maxValueInList(listForTest);
        System.out.println("Maximal of those values: "+maxValue);
        int minValue=methods.minValueInList(listForTest);
        System.out.println("Minimal of those values: "+minValue);
        int sumOfElements=methods.sumOflistValues(listForTest);
        System.out.println("Sum of those values: "+sumOfElements);
        List<Integer>bigetThanAverege=methods.countOfValuesCountBiggerThanAverage(listForTest);
        bigetThanAverege.forEach(p->{
            System.out.print(p+" ");
        });
    }
}
