package task05_Lambdas_StreamsThirdTask;

import java.util.List;

public interface StreamsMethods {
    int averageOfListValues(List<Integer>list);

    int maxValueInList(List<Integer>list);

    int minValueInList(List<Integer>list);

    int sumOflistValues(List<Integer>list);

    List<Integer> countOfValuesCountBiggerThanAverage(List<Integer> list);

}
