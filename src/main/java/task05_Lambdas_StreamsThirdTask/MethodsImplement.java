package task05_Lambdas_StreamsThirdTask;

import java.util.List;
import java.util.function.ToIntFunction;
import java.util.stream.Collectors;

public class MethodsImplement implements StreamsMethods {
    @Override
    public int averageOfListValues(List<Integer> list) {
        int counter = (int) list.stream().count();
        int sum = sumOflistValues(list);
        return sum / counter;
    }

    @Override
    public int maxValueInList(List<Integer> list) {
        return list.stream().max((a, b) -> (a - b)).get();
    }

    @Override
    public int minValueInList(List<Integer> list) {
        return list.stream().min((a, b) -> (a - b)).get();
    }

    @Override
    public int sumOflistValues(List<Integer> list) {

        return list.stream().mapToInt(Integer::intValue).sum();
    }

    @Override
    public List<Integer> countOfValuesCountBiggerThanAverage(List<Integer> list) {
        int average = averageOfListValues(list);
        List<Integer> listBiggestValues = list.stream().filter(p -> p > average).collect(Collectors.toList());
        return listBiggestValues;
    }
}

