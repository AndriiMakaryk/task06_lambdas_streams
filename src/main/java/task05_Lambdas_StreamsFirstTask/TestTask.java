package task05_Lambdas_StreamsFirstTask;

public class TestTask {
    public static void main(String[]args){

        System.out.println("The max value is: ");
        SimpleCalculator calculator=(a,b,c)->{
            int maxValue=Math.max(a,Math.max(b,c));
            return maxValue;
        };
        System.out.println(calculator.makeCalculation(24,35,13));
        System.out.println("The average value is: ");
        SimpleCalculator calculator2=(a,b,c)-> {
            return (a+b+c)/3;
        };
        System.out.println(calculator2.makeCalculation(12,31,48));

    }
}
