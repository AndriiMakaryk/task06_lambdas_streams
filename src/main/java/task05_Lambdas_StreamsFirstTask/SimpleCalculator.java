package task05_Lambdas_StreamsFirstTask;

@FunctionalInterface
public interface SimpleCalculator {
    public int makeCalculation(int num1, int num2,int num3);

}
