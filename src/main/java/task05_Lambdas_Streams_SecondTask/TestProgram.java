package task05_Lambdas_Streams_SecondTask;

import java.util.Scanner;

public class TestProgram {
    public static void main(String[] args) {
        int chosenOption = 0;
        Scanner scan = new Scanner(System.in);
        Reciver reciver = new Reciver();
        User user = new User(new CommandLambdaFunction(reciver),
                new CommandMethodReference(reciver),
                new CommandAnonimousClass(reciver),
                new CommandAsObject(reciver));
        do {
            System.out.println();
            System.out.println("-------Menu--------");
            System.out.println("Lambda function press 1.");
            System.out.println("Method refrence press 2.");
            System.out.println("Anonimus class press 3");
            System.out.println("As Object press 4");
            System.out.println("Exit 5");
            chosenOption = scan.nextInt();
            if (chosenOption == 1) {
                user.lambdaFunction();
            } else if (chosenOption == 2) {
                user.methodReference();
            } else if (chosenOption == 3) {
                user.anonimousClass();
            } else if (chosenOption == 4) {
                user.asObject();
            }

        } while (chosenOption != 5);
        scan.close();
    }
}
