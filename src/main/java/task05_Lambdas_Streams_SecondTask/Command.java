package task05_Lambdas_Streams_SecondTask;

public interface Command {
    void execute();
}
