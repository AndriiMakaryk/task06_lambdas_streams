package task05_Lambdas_Streams_SecondTask;

public class CommandAsObject implements Command {

    private Reciver reciver;

    public CommandAsObject(){}

    public CommandAsObject(Reciver reciver) {
        this.reciver = reciver;
    }

    public Reciver getReciver() {
        return reciver;
    }

    public void setReciver(Reciver reciver) {
        this.reciver = reciver;
    }

    @Override
    public void execute() {
        reciver.asObjectClassCommand();
    }
}
