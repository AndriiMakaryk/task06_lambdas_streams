package task05_Lambdas_Streams_SecondTask;

class CommandMethodReference implements Command {

    private Reciver reciver;

    public CommandMethodReference(Reciver reciver) {
        this.reciver = reciver;
    }

    public Reciver getReciver() {
        return reciver;
    }

    public void setReciver(Reciver reciver) {
        this.reciver = reciver;
    }

    @Override
    public void execute() {
        reciver.methosReference();
    }
}
