package task05_Lambdas_Streams_SecondTask;

import java.util.Arrays;
import java.util.List;

public class Reciver {
    void lambdaFunctionMethod() {
        List<Integer> numbers = Arrays.asList(21, 12, 37, 74, 58);
        numbers.forEach(number -> System.out.print(number+" "));
    }

    void methosReference() {
        List<Integer> numbers = Arrays.asList(121, 112, 137, 174, 158);
        numbers.forEach(System.out::println);
    }

    void asAnonimusClass() {
        {
            List<Integer> numbers = Arrays.asList(221, 212, 237, 274, 258);
            for (Integer element : numbers) {
                System.out.print(element + " ");
            }
        }

    }

    void asObjectClassCommand() {
        Command command = new CommandAsObject();
        System.out.println("Maked new Command object: "+command);
    }
}