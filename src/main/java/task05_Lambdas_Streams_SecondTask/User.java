package task05_Lambdas_Streams_SecondTask;


public class User {
    CommandLambdaFunction commandOne;
    CommandMethodReference commandTwo;
    CommandAnonimousClass commandThree;
    CommandAsObject commandFour;

    public User(CommandLambdaFunction commandOne,
                CommandMethodReference commandTwo,
                CommandAnonimousClass commandThree,
                CommandAsObject commandFour) {

        this.commandOne = commandOne;
        this.commandTwo = commandTwo;
        this.commandThree = commandThree;
        this.commandFour = commandFour;
    }

    void lambdaFunction() {
        commandOne.execute();
    }

    void methodReference() {
        commandTwo.execute();
    }

    void anonimousClass() {
        commandThree.execute();
    }

    void asObject() {
        commandFour.execute();
    }

}
